package com.daffodil.tawtinicustom;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * com.daffodil.tawtinicustom . GridAdapter
 * Created by Vishu Bhardwaj on 23/3/17
 * for http://www.daffodilsw.com
 * Copyright (c) 2017 Daffodil Software Ltd. All rights reserved.
 */
public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<GridModel> mData;

    public GridAdapter(Context context, ArrayList<GridModel> data) {
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public int getCount() {
        return this.mData.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rootView = View.inflate(this.mContext, R.layout.grid_item, null);
        ImageView imgIcon = (ImageView) rootView.findViewById(R.id.imgIcon);
        imgIcon.setImageDrawable(this.mData.get(i).getImageDraw());

        Typeface customFontRegular = Typeface.createFromAsset(this.mContext.getApplicationContext().getAssets(),  "fonts/Montserrat-Regular.ttf");
        TextView txtText = (TextView) rootView.findViewById(R.id.txtText);
        txtText.setText(this.mData.get(i).getText());
        txtText.setTypeface(customFontRegular);
        return rootView;
    }
}
