package com.daffodil.tawtinicustom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * com.daffodil.tawtinicustom . HomeElement
 * Created by Vishu Bhardwaj on 23/3/17
 * for http://www.daffodilsw.com
 * Copyright (c) 2017 Daffodil Software Ltd. All rights reserved.
 */
public class HomeElement extends RelativeLayout {

    private Context mContext;


    private String mMainImageUrl;
    private String mTitle;
    private String mDescription;
    private ArrayList<GridModel> mData;

    public HomeElement(Context context) {
        super(context);
        this.mContext = context;
    }

    public HomeElement(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setMainImageUrl (String url) {
        this.mMainImageUrl = url;
    }

    public void setTitle (String title) {
        this.mTitle = title;
    }

    public void setDescription (String description) {
        this.mDescription = description;
    }

    public void setGridData (ArrayList<GridModel> data) {
        this.mData = data;
    }

    public void init () {
        Typeface customFontRegular = Typeface.createFromAsset(this.mContext.getApplicationContext().getAssets(),  "fonts/Montserrat-Regular.ttf");

        View rootView = inflate(this.mContext, R.layout.home_element, this);
        TextView txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
        txtTitle.setText(this.mTitle);
        txtTitle.setTypeface(customFontRegular);

        ImageView imgMainBg = (ImageView) rootView.findViewById(R.id.imgMainBg);
        Picasso.with(this.mContext).load(this.mMainImageUrl).placeholder(R.drawable.bg_img_placeholder).into(imgMainBg);

        TextView txtDescription = (TextView) rootView.findViewById(R.id.txtDescription);
        txtDescription.setText(this.mDescription);
        txtDescription.setTypeface(customFontRegular);

        GridView grdOptions = (GridView) rootView.findViewById(R.id.grdOptions);
        GridAdapter adapter = new GridAdapter(this.mContext, this.mData);
        grdOptions.setAdapter(adapter);
    }

}
