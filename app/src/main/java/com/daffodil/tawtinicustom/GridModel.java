package com.daffodil.tawtinicustom;

import android.graphics.drawable.Drawable;

/**
 * com.daffodil.tawtinicustom . GridModel
 * Created by Vishu Bhardwaj on 23/3/17
 * for http://www.daffodilsw.com
 * Copyright (c) 2017 Daffodil Software Ltd. All rights reserved.
 */
public class GridModel {
    private String mImageSource;
    private String mText;
    private Drawable mImageDraw;

    public GridModel (String imageSource, String text) {
        this.mImageSource = imageSource;
        this.mText = text;
    }

    public GridModel (Drawable imageDraw, String text) {
        this.mImageDraw = imageDraw;
        this.mText = text;
    }

    public void setImageSource (String imageSource) {
        this.mImageSource = imageSource;
    }

    public void setText (String text) {
        this.mText = text;
    }

    public void setImageDraw (Drawable imageDraw) {
        this.mImageDraw = imageDraw;
    }

    public String getImageSource () {
        return this.mImageSource;
    }

    public String getText () {
        return this.mText;
    }

    public Drawable getImageDraw () {
        return this.mImageDraw;
    }
}
