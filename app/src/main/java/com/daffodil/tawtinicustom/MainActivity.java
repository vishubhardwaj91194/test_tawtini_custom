package com.daffodil.tawtinicustom;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        HomeElement custHomeElem = (HomeElement) findViewById(R.id.custHomeElem);
        custHomeElem.setMainImageUrl("http://www.chinaatlanta.org/en/images/flash_pic4/images/1.jpg");
        custHomeElem.setTitle("Al - Qairwan");
        custHomeElem.setDescription("New York University Abu Dhabi, Saadiyat Island Abu Dhabi, UAE ,Building name");

        ArrayList<GridModel> data = new ArrayList<>();
        data.add(new GridModel(getResources().getDrawable(R.drawable.fish),"Fishing"));
        data.add(new GridModel(getResources().getDrawable(R.drawable.sport),"Sports"));
        data.add(new GridModel(getResources().getDrawable(R.drawable.farm),"Farming"));
        data.add(new GridModel(getResources().getDrawable(R.drawable.vr),"3D-VR"));
        data.add(new GridModel(getResources().getDrawable(R.drawable.slide),"Slides"));
        custHomeElem.setGridData(data);

        custHomeElem.init();
    }
}
